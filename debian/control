Source: golang-github-grafana-dskit
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Mathias Gibbens <gibmat@debian.org>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-etcd-server-dev,
               golang-github-alecthomas-units-dev,
               golang-github-alicebob-miniredis-dev,
               golang-github-armon-go-metrics-dev,
               golang-github-aws-aws-sdk-go-dev,
               golang-github-cespare-xxhash-dev,
               golang-github-cristalhq-hedgedhttp-dev,
               golang-github-davecgh-go-spew-dev,
               golang-github-facette-natsort-dev,
               golang-github-felixge-httpsnoop-dev,
               golang-github-go-kit-log-dev,
               golang-github-go-redis-redis-dev (>= 8.11.5),
               golang-github-gogo-googleapis-dev,
               golang-github-gogo-status-dev,
               golang-github-gorilla-mux-dev,
               golang-github-grafana-gomemcache-dev,
               golang-github-grafana-pyroscope-go-godeltaprof-dev,
               golang-github-hashicorp-consul-dev,
               golang-github-hashicorp-go-cleanhttp-dev,
               golang-github-hashicorp-go-sockaddr-dev,
               golang-github-hashicorp-golang-lru-v2-dev,
               golang-github-hashicorp-memberlist-dev,
               golang-github-miekg-dns-dev,
               golang-github-opentracing-contrib-go-grpc-dev,
               golang-github-opentracing-contrib-go-stdlib-dev,
               golang-github-opentracing-opentracing-go-dev,
               golang-github-pkg-errors-dev,
               golang-github-pmezard-go-difflib-dev,
               golang-github-prometheus-client-model-dev,
               golang-github-prometheus-common-dev,
               golang-github-prometheus-exporter-toolkit-dev,
               golang-github-sercand-kuberesolver-dev,
               golang-github-soheilhy-cmux-dev,
               golang-github-stretchr-testify-dev,
               golang-github-uber-jaeger-client-go-dev,
               golang-github-uber-jaeger-lib-dev,
               golang-go.uber-atomic-dev,
               golang-gogoprotobuf-dev,
               golang-golang-x-exp-dev,
               golang-golang-x-net-dev,
               golang-golang-x-sync-dev,
               golang-golang-x-time-dev,
               golang-google-grpc-dev,
               golang-gopkg-yaml.v3-dev,
               golang-goprotobuf-dev,
               golang-prometheus-client-dev,
               golang-snappy-go-dev,
               golang-uber-goleak-dev,
               golang-yaml.v2-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-grafana-dskit
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-grafana-dskit.git
Homepage: https://github.com/grafana/dskit
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/grafana/dskit

Package: golang-github-grafana-dskit-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-etcd-server-dev,
         golang-github-alecthomas-units-dev,
         golang-github-alicebob-miniredis-dev,
         golang-github-armon-go-metrics-dev,
         golang-github-aws-aws-sdk-go-dev,
         golang-github-cespare-xxhash-dev,
         golang-github-cristalhq-hedgedhttp-dev,
         golang-github-davecgh-go-spew-dev,
         golang-github-facette-natsort-dev,
         golang-github-felixge-httpsnoop-dev,
         golang-github-go-kit-log-dev,
         golang-github-go-redis-redis-dev (>= 8.11.5),
         golang-github-gogo-googleapis-dev,
         golang-github-gogo-status-dev,
         golang-github-gorilla-mux-dev,
         golang-github-grafana-gomemcache-dev,
         golang-github-grafana-pyroscope-go-godeltaprof-dev,
         golang-github-hashicorp-consul-dev,
         golang-github-hashicorp-go-cleanhttp-dev,
         golang-github-hashicorp-go-sockaddr-dev,
         golang-github-hashicorp-golang-lru-v2-dev,
         golang-github-hashicorp-memberlist-dev,
         golang-github-miekg-dns-dev,
         golang-github-opentracing-contrib-go-grpc-dev,
         golang-github-opentracing-contrib-go-stdlib-dev,
         golang-github-opentracing-opentracing-go-dev,
         golang-github-pkg-errors-dev,
         golang-github-pmezard-go-difflib-dev,
         golang-github-prometheus-client-model-dev,
         golang-github-prometheus-common-dev,
         golang-github-prometheus-exporter-toolkit-dev,
         golang-github-sercand-kuberesolver-dev,
         golang-github-soheilhy-cmux-dev,
         golang-github-stretchr-testify-dev,
         golang-github-uber-jaeger-client-go-dev,
         golang-github-uber-jaeger-lib-dev,
         golang-go.uber-atomic-dev,
         golang-gogoprotobuf-dev,
         golang-golang-x-exp-dev,
         golang-golang-x-net-dev,
         golang-golang-x-sync-dev,
         golang-golang-x-time-dev,
         golang-google-grpc-dev,
         golang-gopkg-yaml.v3-dev,
         golang-goprotobuf-dev,
         golang-prometheus-client-dev,
         golang-snappy-go-dev,
         golang-uber-goleak-dev,
         golang-yaml.v2-dev,
         ${misc:Depends}
Description: Distributed systems kit (library)
 Grafana Dskit
 .
 This library contains utilities that are useful for building distributed
 services.
 .
 Current state
 .
 This library is still in development. Continuously moving utilities from
 the all the Grafana database-related projects.
 .
  * Mimir (https://github.com/grafana/mimir)
  * Loki (https://github.com/grafana/loki)
  * Tempo (https://github.com/grafana/tempo)
  * Phlare (https://github.com/grafana/phlare)
 .
 Go version compatibility
 .
 This library aims to support at least the two latest Go minor releases.
 .
 Contributing
 .
 If you're interested in contributing to this project:
 .
  * Start by reading the Contributing guide (/CONTRIBUTING.md).
 .
 License
 .
 Apache 2.0 License (https://github.com/grafana/dskit/blob/main/LICENSE)

